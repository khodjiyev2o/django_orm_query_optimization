from django.shortcuts import get_object_or_404, render

from .models import Item


def item_list(request):
    items = Item.objects.select_related('category').filter(is_on_main=True).prefetch_related('tags')
    context = {
        'items': items,
    }
    return render(request, 'catalog/item_list.html', context)


# Самостоятельная работа
def item_detail(request, item_pk):
    item = get_object_or_404(Item.objects.select_related('category').prefetch_related('tags'),  pk=item_pk)

    context = {
        'item': item,
    }
    return render(request, 'catalog/item_detail.html', context)


# https://docs.djangoproject.com/en/dev/topics/http/shortcuts/#get-object-or-404
# https://github.com/django/django/blob/f83b44075dafa429d59e8755aa47e15577cc49f9/django/db/models/query.py